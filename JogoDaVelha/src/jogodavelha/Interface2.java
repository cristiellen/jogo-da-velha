package jogodavelha;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.TimeUnit;
import static javafx.scene.input.KeyCode.J;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Interface2 {

    private JPanel linha1;
    private JPanel linha2;
    private JPanel linha3;
    private JTextField um;
    private JTextField dois;
    private JTextField tres;
    private JTextField quatro;
    private JTextField cinco;
    private JTextField seis;
    private JTextField sete;
    private JTextField oito;
    private JTextField nove;
    private JFrame janela2;
    private boolean jogada;
    private String x;
    private String o;
    private int vetor[];
    private int contador;
    public void construir(String x, String o, boolean j) {

        this.jogada = j;
        this.x=x;
        this.o=o;
        this.vetor= new int[9];
            for (int i=0; i<9; i++)
            {
                vetor[i]=0;
            }
        janela2 = new JFrame(x + " X " + o);
        janela2.setBounds(750, 300, 350, 360);
        janela2.getContentPane().setLayout(new GridLayout(3, 3));
        linha1 = new JPanel();
        linha2 = new JPanel();
        linha3 = new JPanel();

        um = new JTextField();
        dois = new JTextField();
        tres = new JTextField();
        quatro = new JTextField();
        cinco = new JTextField();
        seis = new JTextField();
        sete = new JTextField();
        oito = new JTextField();
        nove = new JTextField();

        um.setPreferredSize(new Dimension(100, 100));
        dois.setPreferredSize(new Dimension(100, 100));
        tres.setPreferredSize(new Dimension(100, 100));
        quatro.setPreferredSize(new Dimension(100, 100));
        cinco.setPreferredSize(new Dimension(100, 100));
        seis.setPreferredSize(new Dimension(100, 100));
        sete.setPreferredSize(new Dimension(100, 100));
        oito.setPreferredSize(new Dimension(100, 100));
        nove.setPreferredSize(new Dimension(100, 100));

        linha1.add(sete);
        linha1.add(oito);
        linha1.add(nove);
        linha2.add(quatro);
        linha2.add(cinco);
        linha2.add(seis);
        linha3.add(um);
        linha3.add(dois);
        linha3.add(tres);

        um.setEditable(false);
        dois.setEditable(false);
        tres.setEditable(false);
        quatro.setEditable(false);
        cinco.setEditable(false);
        seis.setEditable(false);
        sete.setEditable(false);
        oito.setEditable(false);
        nove.setEditable(false);

        um.setBackground(Color.cyan);
        dois.setBackground(Color.CYAN);
        tres.setBackground(Color.CYAN);
        quatro.setBackground(Color.CYAN);
        cinco.setBackground(Color.CYAN);
        seis.setBackground(Color.CYAN);
        sete.setBackground(Color.CYAN);
        oito.setBackground(Color.CYAN);
        nove.setBackground(Color.CYAN);

        janela2.getContentPane().add(linha1);
        janela2.getContentPane().add(linha2);
        janela2.getContentPane().add(linha3);
        //janela2.setUndecorated(true);
        janela2.setResizable(false);
        janela2.setDefaultCloseOperation(EXIT_ON_CLOSE);
        janela2.setVisible(true);
        
        acoesBotoes();
        acoesDoMouse();
      

    }
    public void cor(JTextField  cor,JTextField  cor2,JTextField  cor3,int num)
    {
        
            cor.setBackground(Color.RED);
            cor2.setBackground(Color.RED);
            cor3.setBackground(Color.RED);
            
               
         
        
       /* if(num==1)
        {
           
             
             JogoEncerrado j = new JogoEncerrado();
             //TimeUnit.SECONDS.sleep(4);
             j.construir(x,o);
             
             janela2.dispose();
        }
        if(num==2)
        {
             
             JogoEncerrado j = new JogoEncerrado();
             //TimeUnit.SECONDS.sleep(4);
             j.construir1(x,o);
             janela2.dispose();
        }*/
 
        
        
    }
 // Boolean velha=false;
    public void desabilitar()
    {
        um.setEnabled(false);
        dois.setEnabled(false);
        tres.setEnabled(false);
        quatro.setEnabled(false);
        cinco.setEnabled(false);
        seis.setEnabled(false);
        sete.setEnabled(false);
        oito.setEnabled(false);
        nove.setEnabled(false);
    
    }
   
    public void tela()
    {
           
            if(sete.getText().equals("X") && oito.getText().equals("X") && nove.getText().equals("X"))
            {
                     cor(sete,oito,nove,1);
                     desabilitar();
                    




            }
             if(quatro.getText().equals("X") && cinco.getText().equals("X") && seis.getText().equals("X"))
            {
                cor(quatro,cinco,seis,1);
                desabilitar();
                   


            }
             if(um.getText().equals("X") && dois.getText().equals("X") && tres.getText().equals("X"))
            {
                cor(um,dois,tres,1);
                desabilitar();


            }
             if(sete.getText().equals("X") && quatro.getText().equals("X") && um.getText().equals("X"))
            {
                cor(sete,quatro,um,1);
                desabilitar();


            }
             if(oito.getText().equals("X") && cinco.getText().equals("X") && dois.getText().equals("X"))
            {
                cor(oito,cinco,dois,1);
                desabilitar();


            }
             if(nove.getText().equals("X") && seis.getText().equals("X") && tres.getText().equals("X"))
            {
                cor(nove,seis,tres,1);
                desabilitar();


            }
             if(sete.getText().equals("X") && cinco.getText().equals("X") && tres.getText().equals("X"))
            {
                cor(sete,cinco,tres,1);
                desabilitar();


            }
             if(nove.getText().equals("X") && cinco.getText().equals("X") && um.getText().equals("X"))
            {
                cor(nove,cinco,um,1);
                desabilitar();


            }
            //para o O
             if (sete.getText().equals("O") && oito.getText().equals("O") && nove.getText().equals("O"))
            {
                     cor(sete,oito,nove,2);
                     desabilitar();


            }
             if(quatro.getText().equals("O") && cinco.getText().equals("O") && seis.getText().equals("O"))
            {
                cor(quatro,cinco,seis,2);
                desabilitar();


            }
            if(um.getText().equals("O") && dois.getText().equals("O") && tres.getText().equals("O"))
            {
                cor(um,dois,tres,2);
                desabilitar();


            }
             if(sete.getText().equals("O") && quatro.getText().equals("O") && um.getText().equals("O"))
            {
                cor(sete,quatro,um,2);
                desabilitar();


            }
             if(oito.getText().equals("O") && cinco.getText().equals("O") && dois.getText().equals("O"))
            {
                cor(oito,cinco,dois,2);
                desabilitar();


            }
             if(nove.getText().equals("O") && seis.getText().equals("O") && tres.getText().equals("O"))
            {
                cor(nove,seis,tres,2);
                desabilitar();

            }
             if(sete.getText().equals("O") && cinco.getText().equals("O") && tres.getText().equals("O"))
            {
                cor(sete,cinco,tres,2);
                desabilitar();


            }
             if(nove.getText().equals("O") && cinco.getText().equals("O") && um.getText().equals("O"))
            {
                cor(nove,cinco,um,2);
                desabilitar();

                        
            }
            /* else 
             {
                 if((um.getText().contains("X") || um.getText().contains("O")) && 
                    (dois.getText().contains("X") ||dois.getText().contains("O"))  && 
                    (tres.getText().contains("X") ||tres.getText().contains("O")) && 
                    (quatro.getText().contains("X") ||quatro.getText().contains("O"))&& 
                    (cinco.getText().contains("X") ||cinco.getText().contains("O")) && 
                    (seis.getText().contains("X") ||seis.getText().contains("O")) && 
                    (sete.getText().contains("X") ||sete.getText().contains("O"))&&
                    (oito.getText().contains("X") ||oito.getText().contains("O"))&& 
                    (nove.getText().contains("X") ||nove.getText().contains("O"))  ) 
                 {
                    JogoEncerrado j = new JogoEncerrado();
                     j.construir3();
                    janela2.dispose();
                 }
             }*/
             
       
       
    }
    public void jogada(JTextField num,int posicao) {
     if(vetor[posicao-1]==0)
     {
        if (jogada) {
            num.setText("X");
            vetor[posicao-1]=1;
            jogada = false;
        } else {
            num.setText("O");
            vetor[posicao-1]=1;
            jogada = true;
        }
        //num.setEnabled(false);
     }
    }
 
     public void acoesDoMouse()
    {
        um.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(um,1);
                tela();
                janela2.requestFocus();
            }
        });
         dois.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(dois,2);
                tela();
                janela2.requestFocus();
            }
        });
          tres.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(tres,3);
                tela();
                janela2.requestFocus();
            }
        });
         quatro.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(quatro,4);
                tela();
                janela2.requestFocus();
            }
        });
          cinco.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(cinco,5);
                tela();
                janela2.requestFocus();
            }
        });
           seis.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(seis,6);
                tela();
                janela2.requestFocus();
            }
        });
            sete.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(sete,7);
                tela();
                janela2.requestFocus();
            }
        });
             oito.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(oito,8);
                tela();
                janela2.requestFocus();
            }
        });
              nove.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e)
            {
                jogada(nove,9);
                tela();
                janela2.requestFocus();
            }
        });
        
    }
   
    public void acoesBotoes() 
    {
         janela2.addKeyListener(new KeyListener() {
           
            public void keyTyped(KeyEvent e) {
                
            }

            public void keyPressed(KeyEvent e) {
                 if (e.getKeyCode() == KeyEvent.VK_7) {
                   
                    jogada(sete,7);
                  
                    tela();
                 
                }
                 if (e.getKeyCode() == KeyEvent.VK_8) {
                    jogada(oito,8);
                
                    tela();
                }
                if (e.getKeyCode() == KeyEvent.VK_9) 
                {
                    jogada(nove,9);
                    
                    tela();
                }
                if (e.getKeyCode() == KeyEvent.VK_4) {
                    jogada(quatro,4);
                    
                    tela();
                }
                 if (e.getKeyCode() == KeyEvent.VK_5) {
                    jogada(cinco,5);
                   
                    tela();
                }
                   if (e.getKeyCode() == KeyEvent.VK_6) {
                    jogada(seis,6);
                                     tela();
                }
                 if (e.getKeyCode() == KeyEvent.VK_1) {
                    jogada(um,1);
                   
                    tela();
                }
                 if (e.getKeyCode() == KeyEvent.VK_2) {
                    jogada(dois,2);
                 
                    tela();
                }
                 if (e.getKeyCode() == KeyEvent.VK_3) {
                    jogada(tres,3);
                  
                    tela();
                }
                 
        
            }
              @Override
            public void keyReleased(KeyEvent e) {
                
            }
        });
         
    }
   
  }




















        //
        /*um.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_1) {
                   
                    jogada(um);
                    tela();
                 
                }
            }
        });

        
        dois.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_2) {
                    jogada(dois);
                    tela();
                }
            }
        });
       

        tres.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_3) {
                    jogada(tres);
                    tela();
                }
            }
        });
        

        quatro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_4) {
                    jogada(quatro);
                    tela();
                }
            }
        });
       

        cinco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_5) {
                    jogada(cinco);
                    tela();
                }
            }
        });
       

        seis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_6) {
                    jogada(seis);
                    tela();
                }
            }
        });
      

        sete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_7) {
                    jogada(sete);
                    tela();
                }
            }
        });
       

        oito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_8) {
                    jogada(oito);
                    tela();
                }
            }
        });
       

        nove.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_9) {
                    jogada(nove);
                    tela();
                }
            }
        });
      
        

    }*/

      
       
    


